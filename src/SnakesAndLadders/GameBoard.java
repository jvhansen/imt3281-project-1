package SnakesAndLadders;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class GameBoard extends JLayeredPane {

    private static final int ROWS = 6;
    private static final int COLUMNS = 6;
    private static final int TILECOUNT = ROWS * COLUMNS;
    private Map<Integer, Tile> tileMap = new HashMap<Integer, Tile>();;
    private Map<Integer, Integer> snakesAndLadders = new HashMap<Integer, Integer>();

    public Player player1;
    public Player player2;

    public GameBoard(ActionListener listener, Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        
        addSnakesAndLadders();

        // Set up JPanel for the game board/all tiles
        JPanel gameBoardPanel = new JPanel();
        this.add(gameBoardPanel, JLayeredPane.DEFAULT_LAYER);
        gameBoardPanel.setLayout(new GridLayout(ROWS, COLUMNS));
        gameBoardPanel.setPreferredSize(new Dimension(700, 500));
        gameBoardPanel.setBounds(0, 0, 700, 500);

        // Fill game board with tiles and store them
        int i = TILECOUNT;
        for (int row = ROWS; row > 0; row--) {
            for (int column = COLUMNS; column > 0; column--) {
                int tileNumber = i % COLUMNS == 0 && row % 2 == 0 ? i - column + 1 : i - COLUMNS + column;

                Tile tile = new Tile(tileNumber);
                tileMap.put(tileNumber, tile);

                gameBoardPanel.add(tile);
                tile.setPreferredSize(new Dimension(70, 55));
                tile.addActionListener(listener);
            }
            i -= COLUMNS;
        }

        // Make it pretty
        gameBoardPanel.setBorder(BorderFactory.createMatteBorder(3, 3, 3, 3, Color.BLACK));
        gameBoardPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

        // Add player icons to first tile
        tileMap.get(1).addPlayer(player1);
        tileMap.get(1).addPlayer(player2);
    }

    // Create visible lines for snakes/ladders
    @Override
    public void paint(final Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(5));

        // Iterate over snakesAndLadders hashmap
        for (Entry<Integer, Integer> snakesAndLadders : snakesAndLadders.entrySet()) {
            // Find start/end points for each snake/ladder
            Point p1 = SwingUtilities.convertPoint(tileMap.get(snakesAndLadders.getKey()), 40, 40, this);
            Point p2 = SwingUtilities.convertPoint(tileMap.get(snakesAndLadders.getValue()), 40, 40, this);

            // Set color based on which way they go
            if (snakesAndLadders.getKey() < snakesAndLadders.getValue()) {
                g2.setColor(Color.GREEN);
            } else {
                g2.setColor(Color.RED);
            }

            // Draw them
            g2.drawLine(p1.x, p1.y, p2.x, p2.y);
        }  
    }

    // S/L button pressed - moves player to the end
    public void movePlayerToEndOfSnakeOrLadder(Player player) {
        int newPosition = snakesAndLadders.get(player.getPosition());
        tileMap.get(player.getPosition()).removePlayer(player);
        player.setPosition(newPosition);
        tileMap.get(player.getPosition()).addPlayer(player);
    }

    // Standard move player logic
    public void movePlayer(Player player, int move) {
        // Function does nothing if player cannot move
        if (player.getPosition()+move > TILECOUNT) {
            return;
        }

        // Move player from old position to new position 
        tileMap.get(player.getPosition()).removePlayer(player);
        player.setPosition(player.getPosition()+move);
        tileMap.get(player.getPosition()).addPlayer(player);
    }

    // Checks if the player has won
    public boolean checkWin(Player player) {
        if (player.getPosition() == TILECOUNT) {
            return true;
        } else {
            return false;
        }
    }

    // Checks if current location is start of snake/ladder
    public boolean isOnSnakeOrLadderStart(Player player) {
        if (snakesAndLadders.containsKey(player.getPosition())) {
            return true;
        }
        return false;
    }

    // Enables tile button
    public void enableButton(Integer tileNr) {
        tileMap.get(tileNr).toggleActive(true);
    }

    // Disables tile button
    public void disableButton(Integer tileNr) {
        tileMap.get(tileNr).toggleActive(false);
    }

    public void resetGameBoard() {
        resetPlayer(player1);
        resetPlayer(player2);
    }

    private void resetPlayer(Player player) {
        tileMap.get(player.getPosition()).removePlayer(player);
        player.setPosition(1);
        tileMap.get(player.getPosition()).addPlayer(player);
    }
    
    // Adds snakes and ladders to hashmap
    private void addSnakesAndLadders() {
        snakesAndLadders.put(3, 16);
        snakesAndLadders.put(18, 29);
        snakesAndLadders.put(14, 26);
        snakesAndLadders.put(21, 8);
        snakesAndLadders.put(35, 13);
        snakesAndLadders.put(27, 10);
    }
}