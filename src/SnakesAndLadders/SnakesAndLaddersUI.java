package SnakesAndLadders;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import SnakesAndLadders.Constants.PlayerTurn;

enum menuActions {
    NEWGAME, RESIGN, EXIT
}

@SuppressWarnings("serial")
public class SnakesAndLaddersUI /*extends JPanel*/ implements Runnable, ActionListener {
    private Die die1 = new Die();
    private Die die2 = new Die();
    private JButton btnRollDiePlayer1;
    private JButton btnRollDiePlayer2;
    private JButton newGameButton;
    private JFrame snakesFrame;
    private JLabel infoLabel = new JLabel(" ");
    private JPanel sidePanel;
    private PlayerTurn currentPlayer = PlayerTurn.PLAYER1;
    private boolean switchPlayer = false;
    private GameBoard gameBoardPanel;


    SnakesAndLaddersUI(Player player1, Player player2) {
        gameBoardPanel = new GameBoard(this, player1, player2);
        btnRollDiePlayer1 = new JButton(player1.playerName + ": Roll die");
        btnRollDiePlayer2 = new JButton(player2.playerName + ": Roll die");
        newGameButton = new JButton("New Game");
        newGameButton.setVisible(false);
        newGameButton.setActionCommand("NEW GAME");
        newGameButton.addActionListener(this);
    }

    @Override
    public void run() {

        // Create and set up the frame window
        snakesFrame = new JFrame("Snakes and Ladders");
        snakesFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        // Create menu bar and content
        JMenuBar menuBar = new JMenuBar();

        JMenu menuFileMenuItem = new JMenu("File");
        JMenuItem newGameMenuItem = new JMenuItem(new AbstractAction("New Game") {
            public void actionPerformed(ActionEvent ae) {
                newGame();
            }
        });
        JMenuItem resetMenuItem = new JMenuItem("Reset game");
        resetMenuItem.setActionCommand("NEW GAME");
        resetMenuItem.addActionListener(this);

        JMenuItem resignMenuItem = new JMenuItem(new AbstractAction("Resign current player") {
            public void actionPerformed(ActionEvent ae) {
                infoLabel.setText(getCurrentPlayer().playerName +" loses...");
                newGameButton.setVisible(true);
                disablePlayerButtons();
            }
        });
        JMenuItem exitMenuItem = new JMenuItem(new AbstractAction("Exit") {
            public void actionPerformed(ActionEvent ae) {
                snakesFrame.dispose();
            }
        });

        JMenu menuHelp = new JMenu("Help");
        JMenuItem instuctions = new JMenuItem(new AbstractAction("How to play") {
            public void actionPerformed(ActionEvent ae) {
                JFrame item = new JFrame("How to play");
                JLabel howToPlay = new JLabel("Play by pressing the 'roll dice' button.\n When you land on the start of a snake or a ladder, press the tile to continue.");
                item.add(howToPlay);
                item.setTitle("How to play");
                item.setSize(700, 300);
                item.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                item.setVisible(true);
            }
        });

        JMenu menuAbout = new JMenu("About");
        JMenuItem gameVersion = new JMenuItem(new AbstractAction("Game Version") {
            public void actionPerformed(ActionEvent ae) {
                JFrame item = new JFrame("Game Version");
                JLabel version = new JLabel("Version: 1.0.0");
                item.add(version);
                item.setTitle("Game Version");
                item.setSize(200, 100);
                item.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                item.setVisible(true);
            }
        });

        JMenuItem author = new JMenuItem(new AbstractAction("Author") {
            public void actionPerformed(ActionEvent ae) {
                JFrame item = new JFrame("Author");
                JLabel author = new JLabel("Author: June Hansen");
                item.add(author);
                item.setTitle("Author");
                item.setSize(200, 100);
                item.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                item.setVisible(true);
            }
        });

        // Set button actions
        btnRollDiePlayer1.setActionCommand(Constants.PlayerTurn.PLAYER1.name());
        btnRollDiePlayer1.addActionListener(this);
        btnRollDiePlayer2.setActionCommand(Constants.PlayerTurn.PLAYER2.name());
        btnRollDiePlayer2.addActionListener(this);

        // Add buttons to menubar headers
        menuFileMenuItem.add(newGameMenuItem);
        menuFileMenuItem.add(resetMenuItem);
        menuFileMenuItem.add(resignMenuItem);
        menuFileMenuItem.add(exitMenuItem);
        menuHelp.add(instuctions);
        menuAbout.add(gameVersion);
        menuAbout.add(author);

        // Add headers to menubar
        menuBar.add(menuFileMenuItem);
        menuBar.add(menuHelp);
        menuBar.add(menuAbout);

        // Define panels to hold UI
        JPanel gamePanel = new JPanel();
        sidePanel = new JPanel(new GridBagLayout());
        JPanel dicePanel = new JPanel();

        // Define GridBagConstraints
        GridBagConstraints constraints = new GridBagConstraints();

        // Set up layout types
        gameBoardPanel.setPreferredSize(new Dimension(800, 550));
        gamePanel.setPreferredSize(new Dimension(1000, 550));
        gamePanel.setLayout(new BoxLayout(gamePanel, BoxLayout.X_AXIS));

        // Add infolabel
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 2;
        sidePanel.add(infoLabel, constraints);

        // Add roll dice buttons
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 1;
        sidePanel.add(btnRollDiePlayer1, constraints);
        constraints.gridx = 1;
        sidePanel.add(btnRollDiePlayer2, constraints);

        // Add dice to side panel
        dicePanel.add(die1);
        dicePanel.add(die2);
        dicePanel.setBorder(new EmptyBorder(20, 5, 20, 5));

        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.gridwidth = 2;
        sidePanel.add(dicePanel, constraints);

        // Add new game button
        constraints.gridx = 0;
        constraints.gridy = 3;
        constraints.gridwidth = 2;
        sidePanel.add(newGameButton, constraints);
        sidePanel.setBorder(new EmptyBorder(5, 25, 5, 25));

        // Add subpanels to gamePanel
        gamePanel.add(gameBoardPanel);
        gamePanel.add(sidePanel);

        // Add everything to frame
        snakesFrame.setJMenuBar(menuBar);
        snakesFrame.add(gamePanel);
        snakesFrame.pack();
        snakesFrame.setLocationRelativeTo(null);
        snakesFrame.setVisible(true);

        // Disable player 2 button to start
        btnRollDiePlayer2.setEnabled(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // Roll die button for player is pressed
        if (e.getActionCommand() == Constants.PlayerTurn.PLAYER1.name()
                || e.getActionCommand() == Constants.PlayerTurn.PLAYER2.name()) {
            var sumDie = rollDie();
            gameLogic(sumDie);
        }
        // Tile button is pressed for snake/ladder - handling from GameBoard
        if (e.getActionCommand() == "Move") {
            Player player = getCurrentPlayer();
            gameBoardPanel.disableButton(player.getPosition());
            gameBoardPanel.movePlayerToEndOfSnakeOrLadder(player);
            reEnableButtons();
            if (switchPlayer)
                changePlayerTurn();
        }
        // New game is pressed from game end or menu
        if (e.getActionCommand() == "NEW GAME") {
            gameBoardPanel.resetGameBoard();
            infoLabel.setText("");
            newGameButton.setVisible(false);
            currentPlayer = PlayerTurn.PLAYER1;
            reEnableButtons();
        }

        // Repain frame to make snakes and ladders behave nicely
        snakesFrame.revalidate();
        snakesFrame.repaint();
    }

    public int rollDie() {
        int die1Value = die1.roll();
        int die2Value = die2.roll();

        // Check if die outcomes are equal
        if (die1Value == die2Value) {
            infoLabel.setText("The dice are the same - go again!");
            switchPlayer = false;
        } else {
            infoLabel.setText(" ");
            switchPlayer = true;
        }

        return die1Value + die2Value;
    }

    private void reEnableButtons() {
        // If player 1's turn, enable player 1 - and opposite
        if (currentPlayer == PlayerTurn.PLAYER1) {
            btnRollDiePlayer1.setEnabled(true);
            btnRollDiePlayer2.setEnabled(false);
        } else {
            btnRollDiePlayer1.setEnabled(false);
            btnRollDiePlayer2.setEnabled(true);
        }
    }

    private void changePlayerTurn() {
        // Set which buttons are active based on player turn
        if (currentPlayer == PlayerTurn.PLAYER1) {
            currentPlayer = PlayerTurn.PLAYER2;
            btnRollDiePlayer1.setEnabled(false);
            btnRollDiePlayer2.setEnabled(true);
        } else {
            currentPlayer = PlayerTurn.PLAYER1;
            btnRollDiePlayer1.setEnabled(true);
            btnRollDiePlayer2.setEnabled(false);
        }
    }

    private void gameLogic(int diceValue) {
        Player player = getCurrentPlayer();

        // Move the player and check for winner
        gameBoardPanel.movePlayer(player, diceValue);
        if (gameBoardPanel.checkWin(player)) {
            disablePlayerButtons();
            infoLabel.setText(player.playerName + " is the winner!");
            newGameButton.setVisible(true);
            return;
        }

        // Check if new tile is snake/ladder start
        if (gameBoardPanel.isOnSnakeOrLadderStart(player)) {
            // Yes: Disable both player buttons, enable tile button
            disablePlayerButtons();
            gameBoardPanel.enableButton(player.getPosition());
        }
        else if (switchPlayer) {
            // No: Switch player turn
            changePlayerTurn();
        }
    }

    private void disablePlayerButtons() {
        btnRollDiePlayer1.setEnabled(false);
        btnRollDiePlayer2.setEnabled(false);
    }

    private Player getCurrentPlayer() {
        if (currentPlayer == PlayerTurn.PLAYER1) {
            return gameBoardPanel.player1;
        } 
        return gameBoardPanel.player2;
    }

    private void newGame() {
        Thread newGame = new Thread(new SaLSetup());
        newGame.start();
        snakesFrame.dispose();
    }
}