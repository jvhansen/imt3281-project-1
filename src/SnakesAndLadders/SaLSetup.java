package SnakesAndLadders;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import java.awt.BorderLayout;
import java.awt.Color;

public class SaLSetup implements Runnable, ActionListener {

    private JFrame setupFrame = new JFrame("Setup - Snakes and Ladders");
    private JLabel player1Label = new JLabel("Player 1 name: ");
    private JLabel player2Label = new JLabel("Player 2 name: ");
    private JTextField player1NameTextField = new JTextField("", 10);
    private JTextField player2NameTextField = new JTextField("", 10);
    private JButton start = new JButton("Start game");
    private Map<String, ImageIcon> playerPieceIcons = new HashMap<String, ImageIcon>();
    private Map<String, JButton> player1PieceButtons = new HashMap<String, JButton>();
    private Map<String, JButton> player2PieceButtons = new HashMap<String, JButton>();
    private Player player1;
    private Player player2;
    private String player1IconKey;
    private String player2IconKey;

    @Override
    public void run() {
        setupFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        // Define panels to hold the buttons
        JPanel mainPanel = new JPanel(new GridBagLayout());
        JPanel player1Panel = new JPanel(new GridBagLayout());
        JPanel player2Panel = new JPanel(new GridBagLayout());
        JPanel player1GamePiecePanel = new JPanel(new GridBagLayout());
        JPanel player2GamePiecePanel = new JPanel(new GridBagLayout());

        var constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.BOTH;

        player1Panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        player2Panel.setBorder(new EmptyBorder(10, 10, 10, 10));

        // Sets size to game buttons panel, get some space
        mainPanel.setPreferredSize(new Dimension(180, 130));

        // Create icon picker
        File directory = new File(".\\src\\Images");
        if (!directory.isDirectory()) {
            System.out.println("The image folder does not exist.");
            return;
        }

        // Get all files ending with png
        File[] files = directory.listFiles((dir, name) -> name.toLowerCase().endsWith(".png"));
        assert files != null;

        constraints.gridwidth = 1;
        int column = 0;
        int row = 0;
        int count = 0;
        for (File file : files) {
            ImageIcon player1Icon = Utility.getScaledImageIcon(new ImageIcon(file.toPath().toString()), 40, 40);
            JLabel player1ButtonLabel = new JLabel(player1Icon);
            JButton player1Choice = new JButton();
            player1Choice.add(player1ButtonLabel);
            player1Choice.setActionCommand("PLAYER1_" + count);
            player1Choice.addActionListener(this);
            player1PieceButtons.put(String.valueOf(count), player1Choice);

            ImageIcon player2Icon = Utility.getScaledImageIcon(new ImageIcon(file.toPath().toString()), 40, 40);
            JLabel player2ButtonLabel = new JLabel(player2Icon);
            JButton player2Choice = new JButton();
            player2Choice.add(player2ButtonLabel);
            player2Choice.setActionCommand("PLAYER2_" + count);
            player2Choice.addActionListener(this);
            player2PieceButtons.put(String.valueOf(count), player2Choice);

            var icon = new ImageIcon(file.toPath().toString());
            playerPieceIcons.put(String.valueOf(count), icon);

            constraints.gridx = column;
            constraints.gridy = row;
            player1GamePiecePanel.add(player1Choice, constraints);
            player2GamePiecePanel.add(player2Choice, constraints);

            if (column >= 3) {
                column = 0;
                row++;
            } else {
                column++;
            }

            count++;
        }

        // Set up frame and stuff correctly
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 1;
        player1Panel.add(player1Label, constraints);
        player2Panel.add(player2Label, constraints);
        mainPanel.add(player1Panel, constraints);

        constraints.gridx = 1;
        constraints.gridy = 0;
        constraints.gridwidth = 1;
        player1Panel.add(player1NameTextField, constraints);
        player2Panel.add(player2NameTextField, constraints);
        mainPanel.add(player2Panel, constraints);

        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 2;
        player1Panel.add(player1GamePiecePanel, constraints);
        player2Panel.add(player2GamePiecePanel, constraints);
        mainPanel.add(start, constraints);

        setupFrame.setMinimumSize(new Dimension(800, 600));
        setupFrame.setMaximumSize(new Dimension(1200, 1200));

        start.setEnabled(false);
        start.addActionListener(this);
        start.setActionCommand("START");

        setupFrame.add(mainPanel, BorderLayout.CENTER);
        setupFrame.pack();
        setupFrame.setLocationRelativeTo(null);
        setupFrame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getActionCommand().startsWith("PLAYER")) {
            handlePlayerChoice(e);
        }

        if (e.getActionCommand() == "START") {
            var player1Name = player1NameTextField.getText();
            if (player1Name != null && !player1Name.isBlank()) {
                player1.playerName = player1Name;
            }

            var player2Name = player2NameTextField.getText();
            if (player2Name != null && !player2Name.isBlank()) {
                player2.playerName = player2Name;
            }

            Thread ui = new Thread(new SnakesAndLaddersUI(player1, player2));
            ui.start();
            setupFrame.dispose();
        }
    }

    private void handlePlayerChoice(ActionEvent e) {
        var command = e.getActionCommand();
        var commandParts = command.split("_");
        String player = commandParts[0];
        String iconChoiceKey = commandParts[1];

        if (player.equals("PLAYER1")) {
            player1 = new Player("Player 1", playerPieceIcons.get(iconChoiceKey));
            player1IconKey = iconChoiceKey;

            resetPlayer1Buttons();
            player1PieceButtons.get(iconChoiceKey).setBackground(Color.GREEN);

            resetPlayer2Buttons();
            player2PieceButtons.get(iconChoiceKey).setEnabled(false);
        } else {
            player2 = new Player("Player 2", playerPieceIcons.get(iconChoiceKey));
            player2IconKey = iconChoiceKey;

            resetPlayer2Buttons();
            player2PieceButtons.get(iconChoiceKey).setBackground(Color.GREEN);

            resetPlayer1Buttons();
            player1PieceButtons.get(iconChoiceKey).setEnabled(false);
        }

        if (player1 != null && player2 != null) {
            start.setEnabled(true);
        }
    }

    private void resetPlayer1Buttons() {
        for (Entry<String, JButton> button : player1PieceButtons.entrySet()) {
            if (!button.getKey().equals(player1IconKey) && !(button.getKey().equals(player2IconKey))) {
                button.getValue().setEnabled(true);
                button.getValue().setBackground(null);
            }
        }
    }

    private void resetPlayer2Buttons() {
        for (Entry<String, JButton> button : player2PieceButtons.entrySet()) {
            if (!button.getKey().equals(player1IconKey) && !(button.getKey().equals(player2IconKey))) {
                button.getValue().setEnabled(true);
                button.getValue().setBackground(null);
            }
        }
    }
}