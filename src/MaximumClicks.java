import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.nio.file.Files;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class MaximumClicks extends JPanel implements Runnable, ActionListener {
	private JLabel clickCounter;
	private JLabel timeLeft;
	private JButton click;
	private JButton start;
	// --------------------------
	int count = 0;
	File fil = new File("record.txt");
	int record = FileReading(fil);

	private enum Actions {
		START, CLICK
	}

	@Override
	public void run() {
		System.out.print(fil.getAbsolutePath());
		JFrame maximumClicksFrame = new JFrame("Maximum Clicks");
		maximumClicksFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		JPanel gamePanel = new JPanel();

		start = new JButton("Start");
		click = new JButton("Click ME!");

		JLabel instruction = new JLabel("See how many clicks you can do in a minute");
		JLabel RecordLabel = new JLabel("Record: " + FileReading(fil));
		clickCounter = new JLabel("0");
		timeLeft = new JLabel("Time Left: - Seconds");

		clickCounter.setBorder(BorderFactory.createDashedBorder(Color.black));
		timeLeft.setBorder(BorderFactory.createLineBorder(Color.black));

		start.setActionCommand(Actions.START.name());
		click.setActionCommand(Actions.CLICK.name());

		start.addActionListener(this);
		click.addActionListener(this);

		click.setEnabled(false);

		gamePanel.add(instruction);
		gamePanel.add(RecordLabel);
		gamePanel.add(start);
		gamePanel.add(click);
		gamePanel.add(clickCounter);
		gamePanel.add(timeLeft);

		maximumClicksFrame.add(gamePanel);

		maximumClicksFrame.pack();
		maximumClicksFrame.setLocationRelativeTo(null);
		maximumClicksFrame.setVisible(true);
		maximumClicksFrame.setSize(300, 200);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand() == Actions.START.name()) {
			count = 0;
			click.setEnabled(true);
			start.setEnabled(false);
			//Using timers to countdown.
			Timer clock = new Timer();
			clock.schedule(new TimerTask() {
				int second = 61;
				//1sec delay

				@Override
				public void run() {

					timeLeft.setText("Time Left: " + --second + " seconds.");
					if (second == 0) {
						clock.cancel();
						timeLeft.setText("Time Left: 0 seconds.");
						click.setEnabled(false);
						start.setEnabled(true);
						if(record < count){
							FileWriting(fil, count);

						}


					}


				}
			}, 0, 1000);
			

		}
		// Teller opp clicks
		else if (e.getActionCommand() == Actions.CLICK.name()) {
			count++;
			clickCounter.setText(count + " clicks");

		}

	}

	//Reads the current record from File.
	private static int FileReading(File file){
		try {
			return Integer.parseInt(Files.readString(file.toPath()));
		} catch (Exception e) {
			return 0;
		}
	}
	//Writes the new highscore to File.
	private static void FileWriting(File file, int count){
		try {
			FileOutputStream fos = new FileOutputStream(file);

			//Create byte array to write to file
			byte[] bytesArray = Integer.toString(count).getBytes();

			fos.write(bytesArray);
			fos.flush();

			try {
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	


}
