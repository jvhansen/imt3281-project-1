package SnakesAndLadders;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.util.HashMap;
import java.util.Random;
import java.util.Map.Entry;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.plaf.metal.MetalButtonUI;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class Tile extends JButton {
    int tileNr;
    JPanel playerIconPanel;
    HashMap<String, Player> players;
    Color thisColor;

    public Tile(int tile) {
        tileNr = tile;

        this.setText(String.valueOf(tileNr));

        // Removes square around tile text
        this.setFocusPainted(false);

        this.setEnabled(false);

        // Set disabled text color to black
        this.setUI(new MetalButtonUI() {
            protected Color getDisabledTextColor() {
                return Color.BLACK;
            }
        });

        // Set size of tile
        this.setSize(new Dimension(55, 55));

        players = new HashMap<String, Player>();

        this.setActionCommand("Move");

        // Create random color to set as background
        Random rand = new Random();
        float hue = rand.nextFloat();
        float saturation = (float) (0.6 + rand.nextFloat() * (1 - 0.6)); 
        float brightness = (float) (0.5 + rand.nextFloat() * (1 - 0.5));
        thisColor = Color.getHSBColor(hue, saturation, brightness);

        this.setBackground(thisColor);
    }

    public void newText(int text) {
        this.setText(String.valueOf(tileNr));
    }

    public void toggleActive(boolean act) {
        this.setEnabled(act);

        if (act) {
            this.setBackground(null);
        } else {
            this.setBackground(thisColor);
        }
    }

    public Tile getTile() {
        return this;
    }

    public void addPlayer(Player player) {
        players.put(player.playerName, player);
        drawPlayerIcons();
    }

    public void removePlayer(Player player) {
        players.remove(player.playerName);
        drawPlayerIcons();
    }

    private void drawPlayerIcons() {
        // Remove old panel to avoid double drawing
        if (playerIconPanel != null) {
            this.remove(playerIconPanel);
        }
        
        playerIconPanel = new JPanel(new GridBagLayout());
        playerIconPanel.setBackground(new Color(0, 0, 0, 0));

        for (Entry<String, Player> player : players.entrySet()) {
            // Add image icon to tile labels
            JLabel label = new JLabel(Utility.getScaledImageIcon(player.getValue().playerIcon, 30, 30));
            label.setBorder(new EmptyBorder(10, 10, 10, 10));
            playerIconPanel.add(label);
        }
        this.add(playerIconPanel);
        refreshTile();
    }

    private void refreshTile() {
        this.revalidate();
        this.repaint();
    }
}