import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class RollADice extends JPanel implements Runnable, ActionListener {

	private JLabel player1Score;
	private JLabel player2Score;
	private JLabel diceValue;
	private JLabel winner;

	int diceThrows = 0; 
	int dice = 0;
	int sumPlayer1 = 0;
	int sumPlayer2 = 0;

	private enum Actions {
		PLAYER1, PLAYER2
	}

	// Define new buttons
	JButton player1 = new JButton("Player 1");
	JButton player2 = new JButton("Player 2");

	@Override
	public void run() {
		// Create and set up the frame window
		JFrame diceGameFrame = new JFrame("Roll a Dice");
		diceGameFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		// Define the panels to hold the buttons
		JPanel gamePanel = new JPanel();
		JPanel playerButtonPanel = new JPanel();
		JPanel playerScorePanel = new JPanel();

		// Set up the layout types
		BoxLayout mainLayout = new BoxLayout(gamePanel, BoxLayout.Y_AXIS);
		gamePanel.setLayout(mainLayout);
		gamePanel.setPreferredSize(new Dimension(250, 180));
		BoxLayout playerButtonLayout = new BoxLayout(playerButtonPanel, BoxLayout.X_AXIS);
		playerButtonPanel.setLayout(playerButtonLayout);
		BoxLayout playerScoreLayout = new BoxLayout(playerScorePanel, BoxLayout.X_AXIS);
		playerScorePanel.setLayout(playerScoreLayout);

		// Set padding
		playerButtonPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
		playerScorePanel.setBorder(new EmptyBorder(10, 10, 10, 10));

		// Create instruction text
		JTextArea instruction = new JTextArea(2, 20);
		instruction.setText("Each player takes turns to roll a dice. The one who scores the highest adter three turns wins the game");
		instruction.setLineWrap(true);
		instruction.setOpaque(false);
		
		// Create labels
		diceValue = new JLabel("Dice Value");
		player1Score = new JLabel("Player 1 score");
		player2Score = new JLabel("Player 2 score");
		winner = new JLabel("");

		// Create borders around player scores
		diceValue.setBorder(BorderFactory.createDashedBorder(Color.black));
		player1Score.setBorder(BorderFactory.createLineBorder(Color.black));
		player2Score.setBorder(BorderFactory.createLineBorder(Color.black));

		// Preferred method for distinguishing each button in listener
		player1.setActionCommand(Actions.PLAYER1.name());
		player2.setActionCommand(Actions.PLAYER2.name());

		// Add a listener to each button
		player1.addActionListener(this);
		player2.addActionListener(this);

		// Align stuff
		instruction.setAlignmentX(Component.CENTER_ALIGNMENT);
		diceValue.setAlignmentX(Component.CENTER_ALIGNMENT);
		winner.setAlignmentX(Component.CENTER_ALIGNMENT);

		// Add buttons to panels
		playerButtonPanel.add(player1);
		playerButtonPanel.add(player2);
		playerScorePanel.add(player1Score);
		playerScorePanel.add(player2Score);

		// Add all the things to the final panel
		gamePanel.add(instruction);
		gamePanel.add(playerButtonPanel);
		gamePanel.add(diceValue);
		gamePanel.add(winner);
		gamePanel.add(playerScorePanel);

		// Add to frame
		diceGameFrame.add(gamePanel);

		diceGameFrame.pack();
		diceGameFrame.setLocationRelativeTo(null);
		diceGameFrame.setVisible(true);

		// Disable player 2 button to start
		player2.setEnabled(false);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Random rand = new Random();
		dice = rand.nextInt(6) + 1;
		diceValue.setText("Dice throw: " + dice);

		if (e.getActionCommand() == Actions.PLAYER1.name()) {
			sumPlayer1 += dice;
			player1Score.setText("Player 1: " + sumPlayer1);
		} else if (e.getActionCommand() == Actions.PLAYER2.name()) {
			sumPlayer2 += dice;
			player2Score.setText("Player 1: " + sumPlayer2);
			diceThrows++;
		}

		if (diceThrows < 3) {
			// Flip button if not finished
			flipButtons();
		} else {
			// Disable buttons when done
			player1.setEnabled(false);
			player2.setEnabled(false);

			// Set winner text
			String winning = (sumPlayer1 > sumPlayer2) ? "Player 1 won!" : (sumPlayer1 < sumPlayer2) ? "Player 2 won!" : "It's a tie!";
			winner.setText(winning);
		}
		
	}

	private void flipButtons() {
		player1.setEnabled(!player1.isEnabled());
		player2.setEnabled(!player2.isEnabled());
	}
}
