package SnakesAndLadders;

import javax.swing.ImageIcon;

public class Player {
    String playerName;
    ImageIcon playerIcon;
    private int currentPosition;

    public Player(String name, ImageIcon icon) {
        playerName = name;
        playerIcon = icon;
        currentPosition = 1; 
    }

    public ImageIcon getIcon() {
        return playerIcon;
    }

    public void setPosition(int newPosition) {
        currentPosition = newPosition;
    }

    public int getPosition() {
        return currentPosition;
    }
}
