import SnakesAndLadders.SaLSetup;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

public class MainMenu implements ActionListener {

	private enum Actions {
		DICEGAME, MAXCLICKGAME, SNAKESGAME, EXIT
	}

	public MainMenu() {

		// Create and set up the frame window
		JFrame mainFrame = new JFrame("Game Center");
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Define new buttons
		JButton diceGameBtn = new JButton("Roll a Dice");
		JButton maxClickBtn = new JButton("Maximum Clicks");
		JButton snakesLaddersBtn = new JButton("Snakes and Ladders");
		JButton exitBtn = new JButton("Exit");

		// Define panels to hold the buttons
		JPanel gamesPanel = new JPanel();
		JPanel exitPanel = new JPanel();

		// Creates Border around the games panel
		gamesPanel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createBevelBorder(BevelBorder.RAISED, Color.gray, Color.black),
				BorderFactory.createBevelBorder(BevelBorder.LOWERED, Color.gray, Color.black)));

		// Set up the box layout
		BoxLayout layoutTop = new BoxLayout(gamesPanel, BoxLayout.Y_AXIS);
		gamesPanel.setLayout(layoutTop);

		// Sets size to game buttons panel, get some space
		gamesPanel.setPreferredSize(new Dimension(180, 130));

		// Preferred method for distinguishing each button in listener
		diceGameBtn.setActionCommand(Actions.DICEGAME.name());
		maxClickBtn.setActionCommand(Actions.MAXCLICKGAME.name());
		snakesLaddersBtn.setActionCommand(Actions.SNAKESGAME.name());
		exitBtn.setActionCommand(Actions.EXIT.name());

		// Add a listener to each button
		diceGameBtn.addActionListener(this);
		maxClickBtn.addActionListener(this);
		snakesLaddersBtn.addActionListener(this);
		exitBtn.addActionListener(this);

		// Aligning buttons to the center of the panel
		diceGameBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
		maxClickBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
		snakesLaddersBtn.setAlignmentX(Component.CENTER_ALIGNMENT);

		// Adding the buttons to the two panels
		gamesPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		gamesPanel.add(diceGameBtn);
		gamesPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		gamesPanel.add(maxClickBtn);
		gamesPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		gamesPanel.add(snakesLaddersBtn);
		exitPanel.add(exitBtn);

		mainFrame.setMinimumSize(new Dimension(200, 200));
		mainFrame.setMaximumSize(new Dimension(400, 300));
		mainFrame.setLayout(new FlowLayout());
		mainFrame.add(gamesPanel, BorderLayout.CENTER);
		mainFrame.add(exitPanel, BorderLayout.SOUTH);
		mainFrame.pack();
		mainFrame.setLocationRelativeTo(null);
		mainFrame.setVisible(true);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand() == Actions.DICEGAME.name()) {
			Thread rollADicsGame = new Thread(new RollADice());
			rollADicsGame.start();
		} else if (e.getActionCommand() == Actions.MAXCLICKGAME.name()) {
			Thread maximumClicksGame = new Thread(new MaximumClicks());
			maximumClicksGame.start();
		}else if (e.getActionCommand() == Actions.SNAKESGAME.name()) {
			Thread snakesGame = new Thread(new SaLSetup());
			snakesGame.start();
		} else {
			System.exit(0);
		}

	}

}
