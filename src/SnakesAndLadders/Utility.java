package SnakesAndLadders;

import java.awt.Image;
import javax.swing.ImageIcon;

public class Utility {
    public static ImageIcon getScaledImageIcon(ImageIcon playerIcon, int width, int height) {
        var scaledImage = playerIcon.getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH);
        return new ImageIcon(scaledImage);
    }
}