package SnakesAndLadders;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.Random;

import javax.swing.JComponent;

// Credit for most of this code to
// https://perso.ensta-paris.fr/~diam//java/online/notes-java/examples/graphics/rolldice/rolldice.html
// https://wiki.scn.sap.com/wiki/display/Snippets/Implementing+dice+roller+using+java+Swing

@SuppressWarnings("serial")
public class Die extends JComponent {
    static final int SPOT_DIAM = 9;
    static final int HEIGHTWIDTH = 50;
    private int faceValue = 1;

    public Die() {
        this.setPreferredSize(new Dimension(HEIGHTWIDTH, HEIGHTWIDTH));
    }

    public int roll() {
        Random rand = new Random();
        faceValue = rand.nextInt(6) + 1;
        repaint();
        return faceValue;
    }

    @Override 
    protected void paintComponent(Graphics g) {
        int w = HEIGHTWIDTH;
        int h = HEIGHTWIDTH;

        // Change to Graphic2D for smoother spots.
        Graphics2D g2 = (Graphics2D)g;
        RenderingHints rh = new RenderingHints(
             RenderingHints.KEY_TEXT_ANTIALIASING,
             RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2.setRenderingHints(rh);

        // Paint background
        g2.setColor(Color.white);
        g2.fillRect(0, 0, w, h);

        // Draw border
        g2.setColor(Color.BLACK);
        g2.drawRect(0, 0, w-1, h-1);

        switch (faceValue) {
            case 1:
                drawSpot(g2, w/2, h/2);
                break;
            case 3:
                drawSpot(g2, w/2, h/2);
                // Fall thru to next case
            case 2:
                drawSpot(g2, w/4, h/4);
                drawSpot(g2, 3*w/4, 3*h/4);
                break;
            case 5:
                drawSpot(g2, w/2, h/2);
                // Fall thru to next case
            case 4:
                drawSpot(g2, w/4, h/4);
                drawSpot(g2, 3*w/4, 3*h/4);
                drawSpot(g2, 3*w/4, h/4);
                drawSpot(g2, w/4, 3*h/4);
                break;
            case 6:
                drawSpot(g2, w/4, h/4);
                drawSpot(g2, 3*w/4, 3*h/4);
                drawSpot(g2, 3*w/4, h/4);
                drawSpot(g2, w/4, 3*h/4);
                drawSpot(g2, w/4, h/2);
                drawSpot(g2, 3*w/4, h/2);
                break;
        }
    }

    private void drawSpot(Graphics g, int x, int y) {
        g.fillOval(x-SPOT_DIAM/2, y-SPOT_DIAM/2, SPOT_DIAM, SPOT_DIAM);
    }

}
